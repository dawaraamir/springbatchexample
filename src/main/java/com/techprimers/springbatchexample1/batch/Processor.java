package com.techprimers.springbatchexample1.batch;

import com.techprimers.springbatchexample1.model.User;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class Processor implements ItemProcessor<User, User> {

    private static final Map<String, String> ACCOUNT_TYPE =
            new HashMap<>();

    public Processor() {
        ACCOUNT_TYPE.put("001", "Checking");
        ACCOUNT_TYPE.put("002", "Savings");
    }

    @Override
    public User process(User user) throws Exception {
        String accountType = user.getAccountType();
        String accType = ACCOUNT_TYPE.get(accountType);
        user.setAccountType(accType);
        user.setTime(new Date());
        System.out.println(String.format("Converted from [%s] to [%s]", accountType, accType));
        return user;
    }
}
